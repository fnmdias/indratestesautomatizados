export const ELEMENTS = {
    botaoMais: '.button-plus',
    botaoMenos: '.button-minus',
    textoSubTotal: "[data-title = 'Total'] span",
    textoValorUnitario: "td.cart_unit span span",
    inputQuantidade: '.cart_quantity input', 
    descricaoProduto: 'td.cart_description',
    botaoDelete: "[title='Delete']", 
    alertaVazio: '.alert'
}