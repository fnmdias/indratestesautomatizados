const el = require('./elements').ELEMENTS;

class ShoppingCartSummary {
    

    aumentarQuantidade(quantEsperada) {
        cy.intercept( {
            method: 'POST',
          }).as('PostAdd')
          cy.get(el.botaoMais).click()
          cy.wait('@PostAdd')
                  .its('response.statusCode').should('eq', 200)
                  .then((xhr) => {
                    cy.get(el.inputQuantidade)
                      .first()
                      .invoke('val')
                      .then((quantEncontrada) => {
                         expect(parseInt(quantEncontrada)).to.equal(quantEsperada)
                      })
                  })
    }

    diminuirQuantidade(quantEsperada) {
        cy.intercept( {
            method: 'POST',
        }).as('PostMinus')
        cy.get(el.botaoMenos).click()
        cy.wait('@PostMinus')
                .its('response.statusCode').should('eq', 200)
                .then((xhr) => {
                cy.get(el.inputQuantidade)
                    .first()
                    .invoke('val')
                    .then((quantEncontrada) => {
                        expect(parseInt(quantEncontrada)).to.equal(quantEsperada)
                    })
                })
    }

    verificarSubTotal(quant) {
        cy.get(el.textoValorUnitario)
            .invoke('text')
            .then((textValorUnitario) => {
                let valorUnitario = parseInt(textValorUnitario.trim().substring(1))
                cy.get(el.textoSubTotal)
                    .invoke('text')
                    .then((textSubTotal) => {
                        let valorSubtotal = parseInt(textSubTotal.trim().substring(1))
                        expect(valorSubtotal).to.equal(quant * valorUnitario)
                
                    })
            })
    }

    removerProduto(produto) {
        cy.intercept( {
            method: 'POST',
        }).as('PostDelete')
        cy.get(el.descricaoProduto)
            .contains(produto)
            .parent().parent().parent()
            .find(el.botaoDelete)
            .click()
        cy.wait('@PostDelete')
            .its('response.statusCode').should('eq', 200)
    }

    verificarCarrinhoVazio() {
        cy.get(el.alertaVazio).should('to.have.text', "Your shopping cart is empty.")
    }


}

export default new ShoppingCartSummary();
