const el = require('./elements').ELEMENTS

class LoginSenha{
 
    acessarPagina() {
        cy.visit("http://automationpractice.com/index.php?controller=authentication&back=my-account")    
    }

    login(email, password) {
        cy.get('#email').type(email)
        cy.get('#passwd').type(password)
        cy.intercept({
            method: 'POST'
        }).as('PostLogin')
        cy.get('#SubmitLogin').click()
        cy.wait('@PostLogin')
            .its('response.statusCode').should('eq', 302)  
    }

    verificarLoginUsuarioInexistente (email, password) {
        cy.get('#email').type(email)
        cy.get('#passwd').type(password)
        cy.intercept({
            method: 'POST'
        }).as('PostLogin')
        cy.get('#SubmitLogin').click()
        cy.wait('@PostLogin')
            .its('response.statusCode').should('eq', 200) 
            .then((xhr) => {
                cy.get('.alert-danger li')
                    .should('have.text','Authentication failed.')
            })
    }

}

export default new LoginSenha()