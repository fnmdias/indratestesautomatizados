const el = require('./elements').ELEMENTS;

class AdicionarProdutoCarrinho {
     
    /*
    Adiciona um produto a partir do botão 'Add Cart'
    É esperado que a resposta a requisição POST seja 200 e que o layer do carrinho seja visível.
    */
    visitarSite() {
        cy.visit('http://automationpractice.com/index.php?id_category=9&controller=category')
    }

    adicionarProduto(produto) {
        cy.intercept( {
            method: 'POST',
        }).as('getPost')
        cy.get(el.blocoNomeProduto)
            .contains(produto)
            .parent().parent()
            .find('[title="Add to cart"]')
            .click()
        cy.wait('@getPost')
            .its('response.statusCode').should('eq', 200)
            .then((xhr) => {
                cy.get(el.layerCart)
                    .should('be.visible')
            })
                
    }
        
    fecharSacola() {
        cy.get(el.botaoCloseWindow).click()
    }

    validarQuantidade(quantProdutos) {
        cy.get('#layer_cart_product_quantity')
        .invoke('text')
        .then((textQuantity) => {
            let quant = parseInt(textQuantity.trim())
            expect(quant).to.equal(quantProdutos)
        })
    }

    validarValorTotalProduto(quant) {
        cy.get(el.precoProduto)
            .invoke("text").then((textProductPrice) => {
                let valorProduto = parseInt(textProductPrice.trim().substring(1))
            
                cy.get(el.Total)
                .invoke('text')
                .then((textTotal) => {
                    let valorTotal = parseInt(textTotal.trim().substring(1))
                    expect(valorTotal).to.equal(quant * valorProduto)
                })
        })
    }

    validarValorTotalCompra(quant) {
        cy.get(el.precoProduto)
            .invoke("text").then((textProductPrice) => {
                let valorProduto = parseInt(textProductPrice.trim().substring(1))
                cy.get('.ajax_block_products_total')  
                .invoke('text')
                .then((textTotalProducts) => {
                    let totalProducts = parseInt(textTotalProducts.trim().substring(1))
                    expect(totalProducts).to.equal(quant * valorProduto)
                    cy.get('.cart-prices .ajax_cart_shipping_cost')  
                        .invoke('text')
                        .then((textTotalShipping) => {
                            let totalShipping = parseInt(textTotalShipping.substring(1))
                            cy.get('.cart-prices .ajax_block_cart_total')
                                .invoke('text')
                                .then((textTotal) => {
                                    let total = parseInt(textTotal.substring(1))
                                    expect(total).to.equal(totalProducts + totalShipping)
                                })
                        })
                })
            })
                    
    }

    continuarComprando() {
        cy.get(el.botaoContinueShopping).click()
        cy.get('#layer_cart')
            .should('not.be.visible')

    }

    irParaCheckout() {
        cy.intercept( {
            url: 'http://automationpractice.com/index.php?controller=order',
        }).as('ircheckout')
        cy.get(el.botaoCheckout).click()
        cy.wait('@ircheckout')
            .its('response.statusCode').should('eq', 200)
    }
}

export default new AdicionarProdutoCarrinho();

