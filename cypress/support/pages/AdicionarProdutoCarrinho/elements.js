export const ELEMENTS = {
    precoProduto: 'div .right-block [itemprop=price]',
    Total: '#layer_cart_product_price',
    botaoCheckout: "[title = 'Proceed to checkout']",
    botaoContinueShopping: "[title = 'Continue shopping']",
    blocoNomeProduto: ".right-block .product-name",
    layerCart: '#layer_cart',
    botaoCloseWindow: "[title='Close window']"
}