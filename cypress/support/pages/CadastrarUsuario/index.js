
const el = require('./elements').ELEMENTS;

class AcessoSistema {
    acessarPagina() {
        cy.visit("http://automationpractice.com/index.php?controller=authentication&back=my-account")    
    }

    preencherEmailParaCadastro(email) {
        cy.get(el.email_create).type(email)
        cy.intercept( {
            method: 'POST',
        }).as('PostEmail')

        cy.get(el.submitCreate).click()

        cy.wait('@PostEmail')
            .its('response.statusCode').should('eq', 200)
          
    }

    preencherCamposCadastro() {
        cy.get(el.id_gender2).check();
        cy.get(el.customFirstName).type('Fernanda')
        cy.get(el.customeLastname).type('Dias')
        cy.get(el.email)
        cy.get(el.password).type('passwd')
        cy.get(el.days).select('31')
        cy.get(el.months).select('July')
        cy.get(el.years).select('2022')
        cy.get(el.newsletter).click()
        cy.get(el.optin).click()

        cy.get(el.firstName)
        cy.get(el.lastName)
        cy.get(el.compant).type('Indra')
        cy.get(el.address1).type('Rua Antônio Rabelo, 161, Miramar')
        cy.get(el.address2).type('não consta')
        cy.get(el.city).type('João Pessoa')
        cy.get(el.state).select('Texas')
        cy.get(el.postCode).type('58000')
        cy.get(el.id_country).type('United States')
        cy.get(el.other).type('testes automatizados')
        cy.get(el.phone).type('9999-0000')
        cy.get(el.phoneMobile).type('9999-9999')
        cy.get(el.alias).type('Rua do EcoBusiness')
        cy.get(el.submitAccount).click()

    }

    verificarEmailJaCadastrado(email) {
        cy.get('#create_account_error li')
            .contains('An account using this email address')
            .should('have.text','An account using this email address has already been registered. Please enter a valid password or request a new one. ')
    }

    verificarEmailInexistente(email) {
        cy.get('#noSlide .page-heading')
            .should('have.text', 'Create an account')
    }
}
export default new AcessoSistema()