// Elementos da página index:

export const ELEMENTS = {
   
    //campo de email e botão submmit
    email_create: '#email_create',
    submitCreate: '#SubmitCreate',

    //campos pessoais
    id_gender2: '#id_gender2',
    customFirstName: '#customer_firstname',
    customeLastname: '#customer_lastname',
    email: '#email',
    password: '#passwd',
    days: '#days',
    months: '#months',
    years: '#years',
    newsletter: '#newsletter',
    optin: '#optin',

    //campos de endereço
    firstName: '#firstname',
    lastName: '#lastname',
    compant:'#company',
    address1: '#address1',
    address2:'#address2',
    city:'#city',
    state: '#id_state',
    postCode:'#postcode',
    id_country:'#id_country',
    other:'#other',
    phone:'#phone',
    phoneMobile: '#phone_mobile',
    alias:'#alias',
    submitAccount:'#submitAccount'

}




