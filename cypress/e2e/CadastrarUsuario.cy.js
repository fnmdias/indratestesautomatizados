/*
Essa especificação possui casos de teste para a tela que permite o usuário realizar seu cadastro
e realizar login no sistema.
*/

import CadastrarUsuario from '../support/pages/CadastrarUsuario';
import LogarSistema from '../support/pages/LogarSistema';

describe('Tela para cadastro e login de usuário', () => {

    /*
    Testa a ação de cadastrar um novo usuário (que ainda não existe) no sistema.
    O teste considera que o email informado no parâmetro não existe no sistema e que todos os campos serão digitados corretamente.
    Os resultados esperados são:
      - Dado o email que ainda não existe, deve ir para a página de cadastro
      - Todos os campos devem ser preenchidos corretamente
      - O servidor responde a requisição de cadastro adequadamente
      
    */
    it(' Cadastra novo usuário (que ainda não exista) no sistema', () => {
        CadastrarUsuario.acessarPagina();
        CadastrarUsuario.preencherEmailParaCadastro('naoexiste20@indra.com')
        CadastrarUsuario.verificarEmailInexistente('naoexiste20@indra.com')
        CadastrarUsuario.preencherCamposCadastro()

    });

    /*
    Testa a ação de cadastrar um  usuário que já existe no sistema.
    O teste considera que o email informado no parâmetro já existe no sistema.
    Os resultados esperados são:
      - Dado o email que já existe, o sistema apresenta um erro informando que a existência do usuário
    */
    it ('Tenta cadastrar um usuário (considerando o email) já existente', () => {
        CadastrarUsuario.acessarPagina()
        CadastrarUsuario.preencherEmailParaCadastro('pessoal@indra.com')
        CadastrarUsuario.verificarEmailJaCadastrado('pessoal@indra.com')
    });

    /*
    Testa a ação de realizar login com um usuário existente no sistema.
    O teste considera que o email e a senha informados nos parâmetros já existem no sistema.
    Os resultados esperados são:
      - O servidor responde a requisição de login adequadamente
      - A página é redirecionada para o local onde o usuário estava anteriormente
    */
    it ('Realiza Login com usuário existente', () => {
        LogarSistema.acessarPagina()
        LogarSistema.login('ipy@indra.com', 'passwd')
    });

    /*
    Testa a ação de realizar login com um usuário inexistente.
    O teste considera que o email e a senha informados nos parâmetros não existem no sistema.
    Os resultados esperados são:
      - O servidor responde a requisição de login adequadamente
      - O sistema apresenta um erro de autenticação.
    */
    it ('Tenta realizar login com usuário inexistente', () => {
        LogarSistema.acessarPagina()
        LogarSistema.verificarLoginUsuarioInexistente('naoexisteainda@indra.com', 'tantofaz')
    })

});