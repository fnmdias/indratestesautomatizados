/*
Essa especificação possui casos de teste para a tela que lista os produtos da loja, 
permitindo o usuário adicionar o produto ao carrinho.
*/

import AdicionarProdutoCarrinho from '../support/pages/AdicionarProdutoCarrinho';

describe('Tela para adicionar produto no carrinho', () => {
  beforeEach(() => {
    AdicionarProdutoCarrinho.visitarSite()
  })


  /*
  Testa a ação de escolher o produto "Printed Dress" a partir do botão 'Add to cart'
  O teste considera que não há produto no carrinho.
  Os resultados esperados são: 
    - a layer do carrinho ficar visível
    - a quantidade de produtos deve ser igual a 1
    - o valor total de produtos deve ser o próprio valor do produto selecionado
    - o valor total da compra deve ser o total de produtos mais o frete (shipping) 
  */
  it ("Adicionar um produto no carrinho", () => {
 
    AdicionarProdutoCarrinho.adicionarProduto('Printed Dress');
    AdicionarProdutoCarrinho.validarQuantidade(1);
    AdicionarProdutoCarrinho.validarValorTotalProduto(1)
    AdicionarProdutoCarrinho.validarValorTotalCompra(1)
  })

   /*
  Testa a ação de escolher o produto "Printed Dress" a partir do botão 'Add to cart' quando já existe um produto no carrinho
  O teste considera que já existe um produto no carrinho.
  Os resultados esperados são: 
    - a layer do carrinho deve ficar visível
    - a quantidade de produtos deve ser igual a 2
    - o valor total de produtos deve ser a soma dos dois produtos
    - o valor total da compra deve ser o total de produtos mais o frete (shipping) 
  */
  it ("Adicionar dois produtos iguais no carrinho", () => {
    AdicionarProdutoCarrinho.adicionarProduto('Printed Dress')
    AdicionarProdutoCarrinho.fecharSacola()
    AdicionarProdutoCarrinho.adicionarProduto('Printed Dress')
    AdicionarProdutoCarrinho.validarQuantidade(2)
    AdicionarProdutoCarrinho.validarValorTotalProduto(2)
    AdicionarProdutoCarrinho.validarValorTotalCompra(2)

  })

    /*
  Testa a ação de escolher o produto "Printed Dress" a partir do botão 'Add to cart' quando já existe dois produtos no carrinho
  O teste considera que já existem dois produtos no carrinho.
  Os resultados esperados são: 
    - a layer do carrinho deve ficar visível
    - a quantidade de produtos deve ser igual a 3
    - o valor total de produtos deve ser a soma dos três produtos
    - o valor total da compra deve ser o total de produtos mais o frete (shipping) 
  */
  it ("Adicionar três produtos no carrinho", () => {
    AdicionarProdutoCarrinho.adicionarProduto('Printed Dress')
    AdicionarProdutoCarrinho.fecharSacola()
    AdicionarProdutoCarrinho.adicionarProduto('Printed Dress')
    AdicionarProdutoCarrinho.fecharSacola()
    AdicionarProdutoCarrinho.adicionarProduto('Printed Dress')
    AdicionarProdutoCarrinho.validarQuantidade(3);
    AdicionarProdutoCarrinho.validarValorTotalProduto(3)
    AdicionarProdutoCarrinho.validarValorTotalCompra(3)

  })

  /*
  Testa a ação de clicar no botão "Continue shopping"
  O teste adiciona um produto e depois clica no botão para continuar comprando.
  Resultados esperados:
    - O sistema fecha a layer do carrinho, retornando para a tela que estava
  */
  it ("Permitir continuar comprando", () => {
    AdicionarProdutoCarrinho.adicionarProduto('Printed Dress')
    AdicionarProdutoCarrinho.continuarComprando()
  })

})