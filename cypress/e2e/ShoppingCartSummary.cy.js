/*
Essa especificação possui casos de teste para a tela ShoppingCartSummary que permite
o usuário alterar as quantidades, bem como excluir os produtos do carrinho.
*/

import AdicionarProdutoCarrinho from '../support/pages/AdicionarProdutoCarrinho';
import ShoppingCartSummary from '../support/pages/ShoppingCartSummary';

describe('Tela do Shopping Cart Summary' , () => {
  beforeEach(() => {
    AdicionarProdutoCarrinho.visitarSite()
    AdicionarProdutoCarrinho.adicionarProduto('Printed Dress')
    AdicionarProdutoCarrinho.irParaCheckout()
  })

  /*
  Testa a ação de aumentar a quantidade de um produto no Cart a partir do botão "+"
  O teste adiciona um produto a cesta inicialmente. O teste adiciona duas vezes para garantir o funcionamento adequado.
  Resultados esperados:
    - Ao clicar no botão "+", o servidor responde normalmente a requisição
    - O Subtotal apresentado na tela corresponde a quantidade do produto em questão
  */
  it ("Permite aumentar a quantidade do produto", () => {

    ShoppingCartSummary.aumentarQuantidade(2)
    ShoppingCartSummary.verificarSubTotal(2)
    ShoppingCartSummary.aumentarQuantidade(3)
    ShoppingCartSummary.verificarSubTotal(3)
  })

  /*
  Testa a ação de diminuir a quantidade de um produto no Cart a partir do botão "-"
  O teste adiciona um produto a cesta inicialmente. O teste adiciona uma vez a quantidade e depois retira um.
  Resultados esperados:
    - Ao clicar no botão "-", o servidor responde normalmente a requisição
    - O Subtotal apresentado na tela corresponde a quantidade do produto em questão
  */
  it ("Permite diminuir a quantidade do produto", () => {
    ShoppingCartSummary.aumentarQuantidade(2)
    ShoppingCartSummary.verificarSubTotal(2)
    ShoppingCartSummary.diminuirQuantidade(1)  
    ShoppingCartSummary.verificarSubTotal(1)
  })

  /*
  Testa a ação de deletar um produto no Cart a partir do ícone de lixeira
  O teste adiciona um produto a cesta inicialmente. 
  Resultados esperados:
    - Ao clicar no ícone de leixeira, o servidor responde normalmente a requisição
    - O sistema apresenta uma mensagem de que o carrinho está vazio. 
  */
  it ("Permite remover um produto", () => {
    ShoppingCartSummary.removerProduto('Printed Dress')
    ShoppingCartSummary.verificarCarrinhoVazio()
  })



})